import { Component, OnInit } from '@angular/core';
import { EmployeurService } from '../../services/employeur/employeur.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
@Component({
  selector: 'app-add-employeur',
  templateUrl: './add-employeur.component.html',
  styleUrls: ['./add-employeur.component.css']
})
export class AddEmployeurComponent implements OnInit {
form : FormGroup;
  constructor(private formBuilder: FormBuilder,
     private employeurService: EmployeurService,
     private router:Router) { }

  ngOnInit() {
    this.form=this.formBuilder.group({
     raisonso:['',Validators.required],
     siret:['',Validators.required],
     codeAPE:['',Validators.required] 
    });
  }
  create(){
    console.log(this.form.value);
    this.employeurService.saveEmployeur(this.form.value).subscribe(()=>{
      this.router.navigate(['/employeur'])
    });
  }
 
}
