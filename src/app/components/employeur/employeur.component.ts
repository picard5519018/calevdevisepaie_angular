import { Component, OnInit } from '@angular/core';
import { EmployeurService } from '../../services/employeur/employeur.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-employeur',
  templateUrl: './employeur.component.html',
  styleUrls: ['./employeur.component.css']
})
export class EmployeurComponent implements OnInit {
employeurs;
  constructor(private employeurService : EmployeurService,
     private router:Router
  ) { }

  ngOnInit() {
    this.getEmployeurs();
  }
  getEmployeurs(){
    this. employeurService.findAllEmployeurs().subscribe(data=>{
      console.log(data);
      this.employeurs=data;
    })
}

}
