import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { SalarieService } from '../../services/salarie/salarie.service';


@Component({
  selector: 'app-edit-salarie',
  templateUrl: './edit-salarie.component.html',
  styleUrls: ['./edit-salarie.component.css']
})
export class EditSalarieComponent implements OnInit {
  editForm: FormGroup;
  constructor(
    private salarieService: SalarieService,
    private router: Router,
    private route: ActivatedRoute,
    private formBuilder: FormBuilder
  ) { }

  ngOnInit() {
    this.editForm = this.formBuilder.group({
      id: [''],
      nom: ['', Validators.required],
      prenom: ['', Validators.required],
      numsecu: ['', Validators.required],
     
    });
    const id = Number(this.route.snapshot.paramMap.get('id'));
    this.salarieService.editSalarie(id).subscribe(data => {
      this.editForm.setValue(data);
    });

  }
  update() {
    console.log(this.editForm.value);

    if (this.editForm.valid) {
      this.salarieService.updateSalarie(this.editForm.value).subscribe(() => {
        this.router.navigate(['/salarie']);
      });
    }
  }

}
