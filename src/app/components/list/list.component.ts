import { Component, OnInit } from '@angular/core';
import { SalarieService } from '../../services/salarie/salarie.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {
  salaries;
  constructor(private salarieService: SalarieService,
    private route: ActivatedRoute,
    private router: Router) { }

  ngOnInit() {
    this.salarieService.findAllSalaries().subscribe(data => {
      console.log(data);
      this.salaries = data;
    });
    this.remove();
  }
  remove() {
    const id = Number(this.route.snapshot.paramMap.get('id'));
    console.log("l'id est:"+id);
    this.salarieService.deleteSalarie(id).subscribe(() => {
      this.router.navigate(['/salaries']);
    });
  }

}
