import { Component, OnInit } from '@angular/core';
//import{ FormGroup, FormBuilder, Validators} from'@angular/forms';
import { SalarieService } from '../../services/salarie/salarie.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-salarie',
  templateUrl: './salarie.component.html',
  styleUrls: ['./salarie.component.css']
})
export class SalarieComponent implements OnInit {
//form: FormGroup;

salarie;
  constructor(private salarieService : SalarieService, private router:Router) { }

  ngOnInit() {
    /*this.form = this.formBuilder.group({
      nom: ['',Validators.required],
      prenom: ['',Validators.required],
      numsecu: ['',Validators.required],

    });*/
    this.getSalaries();
  }
 getSalaries(){
this.salarieService.findAllSalaries().subscribe(data=>{
  console.log(data);
  this.salarie=data;
})
 }
  /*create(){
    console.log(this.form.value);
    this.salarieService.saveSalarie(this.form.value).subscribe(() => {
      this.router.navigate(['/salarie']);
    });
  }*/

}
