import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators} from '@angular/forms';
import { EmployeurService } from '../../services/employeur/employeur.service';
import { Router,ActivatedRoute } from '@angular/router';


@Component({
  selector: 'app-list-employeur',
  templateUrl: './list-employeur.component.html',
  styleUrls: ['./list-employeur.component.css']
})
export class ListEmployeurComponent implements OnInit {
employeurs;
  constructor(private employeurService: EmployeurService,
    private router: Router,
    private route: ActivatedRoute,
    private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.employeurService.findAllEmployeurs().subscribe(data =>{
      console.log(data);
      this.employeurs=data;
    });
    this.remove();
  }
  remove(){
    const id = Number(this.route.snapshot.paramMap.get('id'));
    console.log("l'id est :"+id);
    this.employeurService.deleteEmployeur(id).subscribe(() => {
      this.router.navigate(['/employeurs']);
    });
  
   
    }

}
