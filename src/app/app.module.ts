import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { AppComponent } from './app.component';
import { SalarieComponent } from './components/salarie/salarie.component';
import { RouterModule, Routes } from '@angular/router';
import { AddComponent } from './components/add/add.component';
import { FormsModule, ReactiveFormsModule} from '@angular/forms';
import { ListComponent } from './components/list/list.component';
import { EmployeurComponent } from './components/employeur/employeur.component';
import { AddEmployeurComponent } from './components/add-employeur/add-employeur.component';
import { EditSalarieComponent } from './components/edit-salarie/edit-salarie.component';
import { ListEmployeurComponent } from './components/list-employeur/list-employeur.component';
import { EditEmployeurComponent } from './components/edit-employeur/edit-employeur.component';



const appRoutes : Routes =[
  { path: 'salarie',component : SalarieComponent },
  { path :'employeur',   component : EmployeurComponent},
  { path: 'salaries/:id', component: ListComponent },
  { path: 'edit/:id', component: EditSalarieComponent },
  { path: '', redirectTo: '/salaries', pathMatch: 'full'},
  { path: 'employeurs/:id', component: ListEmployeurComponent },
  { path: '', redirectTo: '/employeurs', pathMatch: 'full'}


]

@NgModule({
  declarations: [
    AppComponent,
    SalarieComponent,
    AddComponent,
    ListComponent,
    EmployeurComponent,
    AddEmployeurComponent,
    EditSalarieComponent,
    ListEmployeurComponent,
    EditEmployeurComponent
   
  
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(appRoutes),
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
