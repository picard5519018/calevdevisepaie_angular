import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Employeur } from 'src/app/models/employeur';

@Injectable({
  providedIn: 'root'
})
export class EmployeurService {
 uriEmployeur="http://localhost:8080/employeurs"
  constructor(private http:HttpClient) { }
  findAllEmployeurs()
  {
    return this.http.get(this.uriEmployeur);
  }
  saveEmployeur(employeur:Employeur){
    return this.http.post(this.uriEmployeur,employeur);
  }
  deleteEmployeur(id:number){
    return this.http.delete("http://localhost:8080/employeurs/"+id);
  }
}
