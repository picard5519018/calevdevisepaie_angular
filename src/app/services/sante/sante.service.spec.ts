import { TestBed } from '@angular/core/testing';

import { SanteService } from './sante.service';

describe('SanteService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SanteService = TestBed.get(SanteService);
    expect(service).toBeTruthy();
  });
});
