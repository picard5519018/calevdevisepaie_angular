import { TestBed } from '@angular/core/testing';

import { ImpotsService } from './impots.service';

describe('ImpotsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ImpotsService = TestBed.get(ImpotsService);
    expect(service).toBeTruthy();
  });
});
