import { TestBed } from '@angular/core/testing';

import { InfosuserService } from './infosuser.service';

describe('InfosuserService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: InfosuserService = TestBed.get(InfosuserService);
    expect(service).toBeTruthy();
  });
});
