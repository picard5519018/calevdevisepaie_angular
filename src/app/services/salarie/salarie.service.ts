import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Salarie } from 'src/app/models/salarie';


@Injectable({
  providedIn: 'root'
})
export class SalarieService {
  
    constructor(private http:HttpClient) { }

  findAllSalaries(){
    return this.http.get("http://localhost:8080/salaries"); 
   }

   saveSalarie(salarie:Salarie){
     return this.http.post("http://localhost:8080/salaries",salarie)

   }
   editSalarie(id: number) {
    return this.http.get("http://localhost:8080/salaries/"+id);
  }
   updateSalarie(salarie:Salarie){
     return this.http.put("http://localhost:8080/salaries",salarie);
   }
   deleteSalarie(id: number) {
    return this.http.delete("http://localhost:8080/salaries/"+id);
   }
}
