import { TestBed } from '@angular/core/testing';

import { RetraiteService } from './retraite.service';

describe('RetraiteService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: RetraiteService = TestBed.get(RetraiteService);
    expect(service).toBeTruthy();
  });
});
